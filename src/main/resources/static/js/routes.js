'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('App').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'templates/home.html'
            })
			.state('tables', {
                url: '/tables',
                templateUrl: 'templates/tables.html'
            });
    }
]);