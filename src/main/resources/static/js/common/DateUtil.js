var dateTimePicker = function ($rootScope) {

        return {
            require: '?ngModel',
            restrict: 'AE',
            scope: {
                pick12HourFormat: '@',
                language: '@',
                useCurrent: '@',
                location: '@',
                format: '@',
                ngModel: '='
            },
            link: function (scope, elem, attrs) {
            	var format = 'YYYY-MM-DD';
            	if(scope.format){
            		format = scope.format;
            	}
                elem.datetimepicker({
                    pick12HourFormat: scope.pick12HourFormat,
                    language: scope.language,
                    useCurrent: scope.useCurrent,
                    format: format
                })
                console.log(scope);

//                //Local event change
                elem.on('blur', function () {
                	scope.ngModel = elem.val();
                })
            }
        };
    }
angular
	.module("DateUtil", [])
		.factory('DateUtil',function($window) {
				return {
					toUTCDate : function(date) {
						var _utc = new Date(date.getUTCFullYear(), date
								.getUTCMonth(), date.getUTCDate(), date
								.getUTCHours(), date.getUTCMinutes(), date
								.getUTCSeconds());
						return _utc;
					},
					millisToUTCDate : function(millis) {
						return toUTCDate(new Date(millis));
					}
				};
		})
	.directive('dateTimePicker', dateTimePicker);